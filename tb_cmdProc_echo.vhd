library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;        -- for addition & counting
use ieee.numeric_std.all;               -- for type conversions
use ieee.math_real.all;                 -- for the ceiling and log constant calculation functions

library std;
use std.env.all;

use work.common_pack.all;

entity tb_cmdProc_echo is

end tb_cmdProc_echo;

architecture tb_cmdProc_echo_arc of tb_cmdProc_echo is

component cmdProc
	port (
		clk : std_logic;
		reset:		in std_logic;
		rxnow:		in std_logic;
		rxData:			in std_logic_vector (7 downto 0);
		txData:			out std_logic_vector (7 downto 0);
		rxdone:		out std_logic;
		ovErr:		in std_logic;
		framErr:	in std_logic;
		txnow:		out std_logic;
		txdone:		in std_logic;
		start: out std_logic;
		numWords_bcd: out BCD_ARRAY_TYPE(2 downto 0);
		dataReady: in std_logic;
		byte: in std_logic_vector(7 downto 0);
		maxIndex: in BCD_ARRAY_TYPE(2 downto 0);
		dataResults: in CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1);
		seqDone: in std_logic
	);
end component;

--Watchdog Timeout.
constant wdto : time := 100 ns;

signal clk : std_logic := '0';
signal rst : std_logic := '0';
signal rxNow : std_logic := '0';
--Capital A.  No chance of an ANNN, L or P since there's no Ns, Ls or Ps.
signal rxData : std_logic_vector(7 downto 0) := X"65";
signal rxDone : std_logic;
signal ovErr : std_logic := '0';
signal framErr : std_logic := '0';
signal txData : std_logic_vector(7 downto 0);
signal txNow : std_logic;
signal txDone : std_logic := '0';
signal start : std_logic;
signal numWords_bcd : BCD_ARRAY_TYPE(2 downto 0);
signal dataReady : std_logic := '0';
signal byte : std_logic_vector(7 downto 0) := X"00";
signal maxIndex : BCD_ARRAY_TYPE(2 downto 0) := (others => X"0");
signal dataResults : CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1) := (others => X"00");
signal seqDone : std_logic := '0';
signal watchdog_time : time := 0 ns;
signal watchdog : boolean := false;

begin

	clk <= not clk after 5 ns;
	rst <= '0', '1' after 20 ns, '0' after 40 ns;
	watchdog <= now > watchdog_time + wdto;

	dut : cmdProc
		port map(
			clk => clk,
			reset => rst,
			rxnow => rxNow,
			rxData => rxData,
			txData => txData,
			rxdone => rxDone,
			ovErr => ovErr,
			framErr => framErr,
			txnow => txNow,
			txdone => txDone,
			start => start,
			numWords_bcd => numWords_bcd,
			dataReady => dataReady,
			byte => byte,
			maxIndex => maxIndex,
			dataResults => dataResults,
			seqDone => seqDone
		);

	rxNow_stimulate : process(clk)
	constant cd_reload : integer := 10;
	variable cmdProc_busy : boolean := false;
	variable cd : integer := cd_reload;
	begin
		if rising_edge(clk) then
			if rst = '1' then
				rxNow <= '0';
				cmdProc_busy := false;
			else
				if cmdProc_busy then
					rxNow <= '0';
					if txNow = '1' then
						cmdProc_busy := false;
						cd := cd_reload;
					end if;
				else
					if cd = 0 then
						rxNow <= '1';
						if rxDone <= '1' then
							rxNow <= '0';
							cmdProc_busy := true;
						end if;
					elsif txNow = '1' then
						cd := cd_reload;
						rxNow <= '0';
					else
						rxNow <= '0';
						cd := cd - 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	rxData_stimulate : process(clk)
	variable seed_0 : integer;
	variable seed_1 : integer;
	variable rxData_rand : real;
	variable character_rand : real;
	variable case_rand : real;
	variable cmdProc_busy : boolean := false;
	variable gen_data : boolean := false;
	begin
		if rising_edge(clk) then
			if rst = '1' then
				rxData <= X"00";
				cmdProc_busy := false;
				gen_data := false;
			else
				if rxDone = '1' then
					cmdProc_busy := true;
				elsif txNow = '1' then
					cmdProc_busy := false;
					gen_data := false;
				elsif cmdProc_busy = false and gen_data = false then
					--Random numbers.
					uniform(seed_0, seed_1, rxData_rand);
					uniform(seed_0, seed_1, character_rand);
					uniform(seed_0, seed_1, case_rand);
					if character_rand < 0.1 and not (rxData = X"41" or rxData = X"61") then
						--Number 10% of the time, but not after 'A' or 'a'.
						rxData <= std_logic_vector(to_unsigned(48 + integer(9.999 * rxData_rand),8));
					else
						--Letter 90% of the time
						if case_rand < 0.5 then
							--Lower case 50% of the time
							rxData <= std_logic_vector(to_unsigned(97 + integer(25.999 * rxData_rand), 8));
						else
							--Upper case 50% of the time
							rxData <= std_logic_vector(to_unsigned(65 + integer(25.999 * rxData_rand), 8));
						end if;
					end if;
					gen_data := true;
				end if;
			end if;
		end if;
	end process;

	txData_check : process(clk)
	variable cmdProc_busy : boolean := false;
	begin
		if rising_edge(clk) then
			if rst = '1' then
				cmdProc_busy := false;
			else
				if rxDone = '1' then
					cmdProc_busy := true;
				elsif cmdProc_busy = true and txNow = '1' then
					--Was processing data we gave it and tried to transmit data
					cmdProc_busy := false;
					assert txData = rxData report "Error: Gave 0x" & to_string(rxData) & " to the cmdProc but got back 0x" & to_string(txData) & "." severity failure;
				elsif txNow = '1' then
					--Was not processing data we gave it.
					cmdProc_busy := false;
				end if;
			end if;
		end if;
	end process;

	txDone_respond : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				--At reset, tx module not ready
				txDone <= '0';
			elsif txDone = '0' then
				--Assert 1 cycle after deassert
				txDone <= '1';
			elsif txNow = '1' then
				--Deassert if we see txNow
				txDone <= '0';
			else
				--1 at all other times
				txDone <= '1';
			end if;
		end if;
	end process;

end tb_cmdProc_echo_arc;