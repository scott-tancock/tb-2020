library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;               -- for type conversions
use ieee.math_real.all;                 -- for the ceiling and log constant calculation functions

library std;
use std.env.all;

use work.common_pack.all;

entity tb_cmdProc_a001 is

end tb_cmdProc_a001;

architecture tb_cmdProc_a001_arc of tb_cmdProc_a001 is

component cmdProc
	port (
		clk : std_logic;
		reset:		in std_logic;
		rxnow:		in std_logic;
		rxData:			in std_logic_vector (7 downto 0);
		txData:			out std_logic_vector (7 downto 0);
		rxdone:		out std_logic;
		ovErr:		in std_logic;
		framErr:	in std_logic;
		txnow:		out std_logic;
		txdone:		in std_logic;
		start: out std_logic;
		numWords_bcd: out BCD_ARRAY_TYPE(2 downto 0);
		dataReady: in std_logic;
		byte: in std_logic_vector(7 downto 0);
		maxIndex: in BCD_ARRAY_TYPE(2 downto 0);
		dataResults: in CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1);
		seqDone: in std_logic
	);
end component;

--Sequence to send.
constant inputSeq : CHAR_ARRAY_TYPE(1 to 4) := (X"41", X"30", X"30", X"31");
--Watchdog Timeout.
constant wdto : time := 100 ns;

signal clk : std_logic := '0';
signal rst : std_logic := '0';
signal rxNow : std_logic := '0';
--Capital A.  No chance of an ANNN, L or P since there's no Ns, Ls or Ps.
signal rxData : std_logic_vector(7 downto 0) := X"65";
signal rxDone : std_logic;
signal ovErr : std_logic := '0';
signal framErr : std_logic := '0';
signal txData : std_logic_vector(7 downto 0);
signal txNow : std_logic;
signal txDone : std_logic := '0';
signal start : std_logic;
signal numWords_bcd : BCD_ARRAY_TYPE(2 downto 0);
signal dataReady : std_logic := '0';
signal byte : std_logic_vector(7 downto 0) := X"00";
signal maxIndex : BCD_ARRAY_TYPE(2 downto 0) := (others => X"0");
signal dataResults : CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1) := (others => X"00");
signal seqDone : std_logic := '0';
signal watchdog_time : time := 0 ns;
signal watchdog : boolean := false;

begin

	clk <= not clk after 5 ns;
	rst <= '0', '1' after 20 ns, '0' after 40 ns;
	watchdog <= now > watchdog_time + wdto;

	dut : cmdProc
		port map(
			clk => clk,
			reset => rst,
			rxnow => rxNow,
			rxData => rxData,
			txData => txData,
			rxdone => rxDone,
			ovErr => ovErr,
			framErr => framErr,
			txnow => txNow,
			txdone => txDone,
			start => start,
			numWords_bcd => numWords_bcd,
			dataReady => dataReady,
			byte => byte,
			maxIndex => maxIndex,
			dataResults => dataResults,
			seqDone => seqDone
		);

	--DO NOT write your design in this style.  It WILL NOT synthesise.
	rx_stimulate : process
	variable exp_result : CHAR_ARRAY_TYPE(0 to 2);
	begin
		wait for 100 ns;
		assert rxDone = '0' report "Error: rxDone did not settle to 0." severity failure;
		--Send 4 bytes
		for i in 1 to 4 loop
			rxData <= inputSeq(i);
			rxNow <= '1';
			watchdog_time <= now;
			wait until not watchdog;
			wait on rxDone, watchdog;
			assert (not watchdog) and rxDone = '1' report "rxDone did not assert within 100 ns." severity failure;
			rxNow <= '0';
			watchdog_time <= now;
			wait on rxDone, watchdog;
			assert (not watchdog) and rxDone = '0' report "rxDone did not deassert within 100 ns" severity failure;
			watchdog_time <= now;
			wait until txNow = '1' or watchdog;
			assert txNow = '1' and (not watchdog) report "txNow did not assert within 100 ns." severity failure;
			watchdog_time <= now;
			wait until txNow = '0' or watchdog;
			assert txNow = '0' and (not watchdog) report "txNow did not deassert within 100 ns" severity failure;
			wait until txDone = '1';
		end loop;
		watchdog_time <= now;
		--Wait for start to occur, give it some time 
		--(reset to 100 ns for each pretty-print character, 
		-- up to 10 characters).
		for i in 1 to 10 loop
			watchdog_time <= now;
			wait until start = '1' or watchdog or txNow = '1';
			if txNow = '1' then
				report "Pretty-printing item " & to_string(i) & "." severity note;
				watchdog_time <= now;
				wait on txNow, watchdog;
				assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100 ns." severity failure;
			else
				assert start = '1' and (not watchdog) report "Start not asserted within 100 ns." severity failure;
				exit;
			end if;
		end loop;
		watchdog_time <= now;
		wait for 50 ns;
		exp_result(0) := dataSequence(0)(7 downto 4);
		exp_result(1) := dataSequence(0)(3 downto 0);
		exp_result(2) := X"20";
		if unsigned(exp_result(0)) > 9 then
			exp_result(0) := std_logic_vector(unsigned(exp_result(0)) + 55);
		else
			exp_result(0) := std_logic_vector(unsigned(exp_result(0)) + 48);
		end if;
		if unsigned(exp_result(1)) > 9 then
			exp_result(1) := std_logic_vector(unsigned(exp_result(1)) + 55);
		else
			exp_result(1) := std_logic_vector(unsigned(exp_result(1)) + 48);
		end if;
		byte <= dataSequence(0);
		dataReady <= '1';
		dataResults <= (3 => dataSequence(0), others => X"00");
		maxIndex <= (X"0", x"0", X"0");
		wait for 10 ns;
		watchdog_time <= now;
		dataReady <= '0';
		seqDone <= '1';
		wait until txNow = '1' or now >= watchdog_time + 10 ns;
		if now >= watchdog_time + 10 ns then
			seqDone <= '0';
			wait until txNow = '1' or watchdog;
		end if;
		
		assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
		assert txData = exp_result(0) or txData = (std_logic_vector(unsigned(exp_result(0)) + X"20"))
			report "txData (0x" & to_string(txData) & 
			") not equal to exp_result(0) (0x" & to_string(exp_result(0)) & ")." 
			severity failure;
		watchdog_time <= now;
		wait until now >= watchdog_time + 10 ns;
		seqDone <= '0';
		wait on txNow, watchdog;
		assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
		watchdog_time <= now;
		wait until txNow = '1' or watchdog;
		assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
		assert txData = exp_result(1) or txData = (std_logic_vector(unsigned(exp_result(1)) + X"20"))
			report "txData (0x" & to_string(txData) & 
			") not equal to exp_result(1) (0x" & to_string(exp_result(1)) & ")." 
			severity failure;
		watchdog_time <= now;
		wait until now >= watchdog_time + 10 ns;
		seqDone <= '0';
		wait on txNow, watchdog;
		assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
		watchdog_time <= now;
		wait until txNow = '1' or watchdog;
		assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
		assert txData = exp_result(2) report "txData (0x" & to_string(txData) & 
			") not equal to exp_result(2) (0x" & to_string(exp_result(1)) & ")." 
			severity failure;
		watchdog_time <= now;
		wait until now >= watchdog_time + 10 ns;
		seqDone <= '0';
		wait on txNow, watchdog;
		assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
		report "Test tb_cmdProc_a001 finished." severity note;
		finish(0);
	end process;

	txDone_respond : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				--At reset, tx module not ready
				txDone <= '0';
			elsif txDone = '0' then
				--Assert 1 cycle after deassert
				txDone <= '1';
			elsif txNow = '1' then
				--Deassert if we see txNow
				txDone <= '0';
			else
				--1 at all other times
				txDone <= '1';
			end if;
		end if;
	end process;

end tb_cmdProc_a001_arc;