# Digital Design Group Project 2020 Testbenches
## Purpose
These testbenches are designed to help you verify whether your design meets the
functionality and timing requirements of the specification provided in the A2
lab manual.

If your design passes all these tests correctly, you can have some confidence
that you are on the way to a working solution.

## Tests
These tests check for the following things: 
* The data processor is pulling data from the data generator through the two-
phase protocol (tb\_dataProc\_twoPhase\*)
* The data processor is collecting the right amount of data and accepting the
correct number of START signals before asserting dataReady and seqDone as 
appropriate (tb\_dataProc\_seqDone\*)
* The data processor correctly passes data from the BYTE signal to the DATA
signal (tb\_dataProc\_dataPass.vhd)
* The command processor accepts data from the rx module 
(tb\_cmdProc\_accept.vhd)
* The command processor echos back data that it receives from the rx module
(tb\_cmdProc\_echo.vhd)
* The command processor accepts the aNNN commands, requests the correct amount
of data from the data processor and prints it over serial (tb\_cmdProc\_a\*).

## Known issues
* Some modules do not report the correct notes.
