library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pack.all;

library std;
use std.env.all;

entity tb_dataProc_seqDone_once is
end tb_dataProc_seqDone_once;

architecture testbench_arc of tb_dataProc_seqDone_once is

component dataConsume
	port (
		clk : in std_logic;
		reset : in std_logic; -- synchronous reset
		start : in std_logic; -- goes high to signal data transfer
		numWords_bcd : in BCD_ARRAY_TYPE(2 downto 0);
		ctrlIn : in std_logic;
		ctrlOut : out std_logic;
		data : in std_logic_vector(7 downto 0);
		dataReady : out std_logic;
		byte : out std_logic_vector(7 downto 0);
		seqDone : out std_logic;
		maxIndex : out BCD_ARRAY_TYPE(2 downto 0);
		dataResults : out CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1) -- index 3 holds the peak
	);
end component;

component dataGen
	port (
		clk : in std_logic;
		reset : in std_logic; -- synchronous reset
		ctrlIn : in std_logic;
		ctrlOut : out std_logic;
		data : out std_logic_vector(7 downto 0)
	);
end component;

	signal clk : std_logic := '0';
	signal rst : std_logic := '0';
	signal start : std_logic := '0';
	signal numWords : BCD_ARRAY_TYPE(2 downto 0) := (X"0",X"0",X"1");
	signal ctrlReq : std_logic;
	signal ctrlAck : std_logic;
	signal data : std_logic_vector(7 downto 0);
	signal dataReady : std_logic;
	signal byte : std_logic_vector(7 downto 0);
	signal seqDone : std_logic;
	signal maxIndex : BCD_ARRAY_TYPE(2 downto 0);
	signal dataResults : CHAR_ARRAY_TYPE(0  to RESULT_BYTE_NUM-1);

	signal timeout : std_logic := '0';

begin

	clk <= not clk after 5 ns;
	rst <= '0' when now < 10 ns else '1' when now < 50 ns else '0';
	start <= '0' when now < 60 ns else '1' when now < 70 ns else '0';
	timeout <= '0' when now < 210 ns else '1';

	dut : dataConsume
		port map(
			clk => clk,
			reset => rst,
			start => start,
			numWords_bcd => numWords,
			ctrlIn => ctrlAck,
			ctrlOut => ctrlReq,
			data => data,
			dataReady => dataReady,
			byte => byte,
			seqDone => seqDone,
			maxIndex => maxIndex,
			dataResults => dataResults
		);

	resp : dataGen
		port map(
			clk => clk,
			reset => rst,
			ctrlIn => ctrlReq,
			ctrlOut => ctrlAck,
			data => data
		);

	test_toggle : process
	begin
		wait on ctrlReq, timeout;
		assert timeout = '0' report "Error: ctrlOut did not toggle within 150 ns of start." severity failure;
		wait until now = 1000 ns;
		report "Simulation tb_dataProc_seqDone_once finished" severity note;
		finish(0);
	end process;

	test_ready : process
	begin
		wait on dataReady, timeout;
		assert timeout = '0' and dataReady = '1' report "Error: dataReady did not assert within 150 ns of start." severity failure;
		wait for 9 ns;
		assert dataReady = '1' report "Error: dataReady not high for a whole clock cycle." severity failure;
		wait for 2 ns;
		assert dataReady = '0' report "Error: dataReady high for more than a whole clock cycle." severity failure;
		wait;
	end process;

	test_seqDone : process
	variable a : integer := 0;
	variable b : integer := 0;
	begin
		wait on dataReady, timeout;
		if seqDone = '1' then
			report "seqDone high at same time as dataReady" severity note;
			a := a+1;
		end if;
		wait for 1 ns;
		if seqDone = '1' then
			report "seqDone high within 1 ns of dataReady" severity note;
			b := b+1;
		end if;
		wait for 9 ns;
		if seqDone = '1' then
			report "seqDone high exactly 1 clock cycle after dataReady" severity note;
			a := a+1;
		end if;
		wait for 1 ns;
		if seqDone = '1' then
			assert a < 2 report "seqDone high for more than a single clock cycle" severity failure;
			report "seqDone high within one clock cycle + 1 ns of dataReady" severity note;
			b := b+1;
		end if;
		wait for 1 ns;
		if seqDone = '1' then
			assert b < 2 report "seqDone high for more than a single clock cycle" severity failure;
		end if;
		wait for 8 ns;
		if seqDone = '1' then
			report "seqDone high exactly 2 clock cycles after dataReady" severity note;
			a := a+1;
		end if;
		wait for 1 ns;
		if seqDone = '1' then
			assert a < 2 report "seqDone high for more than a single clock cycle" severity failure;
			report "seqDone high within two clock cycles + 1 ns of dataReady" severity note;
			b := b+1;
		end if;
		if seqDone = '1' then
			assert b < 2 report "seqDone high for more than a single clock cycle" severity failure;
		end if;
		assert a = 2 or b = 2 report "seqDone not high for a full clock cycle" severity failure;
		wait;
	end process;

end testbench_arc;