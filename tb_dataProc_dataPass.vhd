library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;        -- for addition & counting
use ieee.numeric_std.all;               -- for type conversions
use ieee.math_real.all;                 -- for the ceiling and log constant calculation functions

library std;
use std.env.all;

use work.common_pack.all;

entity tb_dataProc_dataPass is
end tb_dataProc_dataPass;

architecture tb_dataProc_dataPass_arc of tb_dataProc_dataPass is

signal clk : std_logic := '0';
signal rst : std_logic := '0';
signal start : std_logic := '0';
signal numWords : BCD_ARRAY_TYPE(2 downto 0);
signal numWords_int : integer range 0 to 999;
signal ctrlReq : std_logic;
signal ctrlAck : std_logic;
signal data : std_logic_vector(7 downto 0);
signal dataReady : std_logic;
signal byte : std_logic_vector(7 downto 0);
signal seqDone : std_logic;
signal maxIndex : BCD_ARRAY_TYPE(2 downto 0);
signal dataResults : CHAR_ARRAY_TYPE(0  to RESULT_BYTE_NUM-1);
signal timeout : std_logic := '0';
signal watchdog_start : time := 0 ns;
signal in_process : boolean := false;

begin

	clk <= not clk after 5 ns;
	--Reset once at 10 ns - 50 ns
	rst <= '0' when now < 10 ns else '1' when now < 50 ns else '0';
	--Max 100 bytes * 10 cycles * 10 ns per cycle = 10,000 ns = 10 us
	timeout <= '0' when now-watchdog_start < 11 us else '1';
	--BCD to integer conversion.  Not great in synthesis.
	numWords_int <= to_integer(unsigned(numWords(0))) 
			+ 10  * to_integer(unsigned(numWords(1))) 
			+ 100 * to_integer(unsigned(numWords(2)));

	--Obtain 100 data bytes at a time
	numWords <= (X"1", X"0", X"0");

	start_stimulate : process(clk)
	variable in_process : boolean := false;
	variable cd : integer := 0;
	constant cd_reload : integer := 4;
	begin
		if rising_edge(clk) then
			if rst = '1' then
				start <= '1';
				in_process := false;
				cd := 0;
			elsif in_process then
				if seqDone = '1' then
					cd := 0;
					in_process := false;
					start <= '0';
				elsif dataReady = '1' then
					cd := cd_reload;
					start <= '0';
				elsif cd = 1 then
					cd := 0;
					start <= '1';
				else
					if not (cd = 0) then
						cd := cd - 1;
					end if;
					start <= '0';
				end if;
			else
				cd := cd_reload;
				in_process := true;
			end if;
		end if;
	end process;

	check_byte : process(clk)
	variable ctr : integer range 0 to SEQ_LENGTH;
	begin
		if rising_edge(clk) then
			if rst = '1' then
				ctr := 0;
			elsif dataReady = '1' then
				if ctr = SEQ_LENGTH-1 then
					ctr := 0;
				else
					ctr := ctr + 1;
				end if;
				assert byte = dataSequence(ctr) report "Error data byte " 
						& to_string(byte) & " not equal to expected byte " 
						& to_string(dataSequence(ctr)) & " at position " & to_string(ctr) & "." 
						severity failure;
			end if;
		end if;
	end process;

	test_seqDone : process
	constant n_tests : integer := 10;
	variable n : integer;
	begin
		for n in 1 to n_tests loop
			wait on seqDone, timeout;
			assert timeout = '0' and seqDone = '1' report "Error: timed out (11 us) waiting for seqDone on test " & to_string(n) & "." severity failure;
			watchdog_start <= now;
		end loop;
		report "Simulation tb_dataProc_dataPass finished" severity note;
		finish(0);
	end process;


end tb_dataProc_dataPass_arc;