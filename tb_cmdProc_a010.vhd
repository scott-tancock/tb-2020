library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;        -- for addition & counting
use ieee.numeric_std.all;               -- for type conversions
use ieee.math_real.all;                 -- for the ceiling and log constant calculation functions

library std;
use std.env.all;

use work.common_pack.all;

entity tb_cmdProc_a010 is

end tb_cmdProc_a010;

architecture tb_cmdProc_a010_arc of tb_cmdProc_a010 is

component cmdProc
	port (
		clk : std_logic;
		reset:		in std_logic;
		rxnow:		in std_logic;
		rxData:			in std_logic_vector (7 downto 0);
		txData:			out std_logic_vector (7 downto 0);
		rxdone:		out std_logic;
		ovErr:		in std_logic;
		framErr:	in std_logic;
		txnow:		out std_logic;
		txdone:		in std_logic;
		start: out std_logic;
		numWords_bcd: out BCD_ARRAY_TYPE(2 downto 0);
		dataReady: in std_logic;
		byte: in std_logic_vector(7 downto 0);
		maxIndex: in BCD_ARRAY_TYPE(2 downto 0);
		dataResults: in CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1);
		seqDone: in std_logic
	);
end component;

function MAX(LEFT, RIGHT: INTEGER) return INTEGER is
begin
  if LEFT > RIGHT then return LEFT;
  else return RIGHT;
    end if;
  end;

function MIN(LEFT, RIGHT: INTEGER) return INTEGER is
begin
  if LEFT < RIGHT then return LEFT;
  else return RIGHT;
    end if;
  end;

--Sequence to send.
constant inputSeq : CHAR_ARRAY_TYPE(1 to 4) := (X"41", X"30", X"31", X"30");
--Watchdog Timeout.
constant wdto : time := 100 ns;

signal clk : std_logic := '0';
signal rst : std_logic := '0';
signal rxNow : std_logic := '0';
--Capital A.  No chance of an ANNN, L or P since there's no Ns, Ls or Ps.
signal rxData : std_logic_vector(7 downto 0) := X"65";
signal rxDone : std_logic;
signal ovErr : std_logic := '0';
signal framErr : std_logic := '0';
signal txData : std_logic_vector(7 downto 0);
signal txNow : std_logic;
signal txDone : std_logic := '0';
signal start : std_logic;
signal numWords_bcd : BCD_ARRAY_TYPE(2 downto 0);
signal dataReady : std_logic := '0';
signal byte : std_logic_vector(7 downto 0) := X"00";
signal maxIndex : BCD_ARRAY_TYPE(2 downto 0) := (others => X"0");
signal dataResults : CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1) := (others => X"00");
signal seqDone : std_logic := '0';
signal watchdog_time : time := 0 ns;
signal watchdog : boolean := false;

begin

	clk <= not clk after 5 ns;
	rst <= '0', '1' after 20 ns, '0' after 40 ns;
	watchdog <= now > watchdog_time + wdto;

	dut : cmdProc
		port map(
			clk => clk,
			reset => rst,
			rxnow => rxNow,
			rxData => rxData,
			txData => txData,
			rxdone => rxDone,
			ovErr => ovErr,
			framErr => framErr,
			txnow => txNow,
			txdone => txDone,
			start => start,
			numWords_bcd => numWords_bcd,
			dataReady => dataReady,
			byte => byte,
			maxIndex => maxIndex,
			dataResults => dataResults,
			seqDone => seqDone
		);

	--DO NOT write your design in this style.  It WILL NOT synthesise.
	rx_stimulate : process
	variable exp_result : CHAR_ARRAY_TYPE(0 to 2);
	variable max_ds : std_logic_vector(7 downto 0) := X"00";
	variable max_idx : integer := 0;
	begin
		wait for 100 ns;
		assert rxDone = '0' report "Error: rxDone did not settle to 0." severity failure;
		--Send 4 bytes
		for i in 1 to 4 loop
			rxData <= inputSeq(i);
			rxNow <= '1';
			watchdog_time <= now;
			wait until (not watchdog);
			wait on rxDone, watchdog;
			assert (not watchdog) and rxDone = '1' report "rxDone did not assert within 100 ns." severity failure;
			rxNow <= '0';
			watchdog_time <= now;
			wait on rxDone, watchdog;
			assert (not watchdog) and rxDone = '0' report "rxDone did not deassert within 100 ns" severity failure;
			watchdog_time <= now;
			wait until txNow = '1' or watchdog;
			assert txNow = '1' and (not watchdog) report "txNow did not assert within 100 ns." severity failure;
			watchdog_time <= now;
			wait until txNow = '0' or watchdog;
			assert txNow = '0' and (not watchdog) report "txNow did not deassert within 100 ns" severity failure;
			wait until txDone = '1';
		end loop;
		watchdog_time <= now;
		--Wait for start to occur, give it some time 
		--(reset to 100 ns for each pretty-print character, 
		-- up to 10 characters).
		for i in 1 to 10 loop
			watchdog_time <= now;
			wait until start = '1' or watchdog or txNow = '1';
			if txNow = '1' then
				report "Pretty-printing item " & to_string(i) & "." severity note;
				watchdog_time <= now;
				wait on txNow, watchdog;
				assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100 ns." severity failure;
			else
				assert start = '1' and (not watchdog) report "Start not asserted within 100 ns." severity failure;
				exit;
			end if;
		end loop;
		--Retrieve 10 bytes
		for i in 0 to 9 loop
			--Wait for start signal
			watchdog_time <= now;
			wait on start, watchdog;
			assert start = '1' and (not watchdog) report "Start not asserted within 100 ns." severity failure;
			--Wait for 50 ns then give dataReady and inputSeq.
			watchdog_time <= now;
			wait for 50 ns;
			--Calculate expected txData
			--First character
			exp_result(0) := dataSequence(i)(7 downto 4);
			--Second character
			exp_result(1) := dataSequence(i)(3 downto 0);
			--Space (ASCII 32 = X"20")
			exp_result(2) := X"20";
			--Adjust to ASCII
			if exp_result(0) > 9 then
				--Adjust into 'A' to 'F' range if > 9
				exp_result(0) := exp_result(0) + 55;
			else
				--Adjust into '0' to '9' range
				exp_result(0) := exp_result(0) + 48;
			end if;
			--Same for second byte
			if exp_result(1) > 9 then
				--Adjust into 'A' to 'F' range if > 9
				exp_result(1) := exp_result(1) + 55;
			else
				--Adjust into '0' to '9' range
				exp_result(1) := exp_result(1) + 48;
			end if;
			--Put onto 'byte' signal
			byte <= dataSequence(i);
			--Signal DR.
			dataReady <= '1';
			--maxResults stuff.  Make sure cmdProc gets correct data.
			if dataSequence(i) > max_ds then
				max_ds := dataSequence(i);
				max_idx := i;
				--results populated 2 cycles ahead (slightly cheating here!)
				for j in max(0,i-2) to min(9,i+2) loop
					dataResults(j+2-i) <= dataSequence(j);
				end loop;
			end if;
			--Turn off DR after 1 clock cycle
			wait for 10 ns;
			watchdog_time <= now;
			dataReady <= '0';

			if i = 9 then
				--If the last cycle, also assert seqDone and wait for txNow
				seqDone <= '1';
				--Not sure whether txNow will come before deasserting seqDone
				wait until txNow = '1' or now >= watchdog_time + 10 ns;
				if now >= watchdog_time + 10 ns then
					--seqDone needs to be deasserted at this moment
					seqDone <= '0';
					--This should fall through if txNow is already '1'
					wait until txNow = '1' or watchdog;
				end if;
			else
				--Just wait for txNow.  Might fall through
				wait until txNow = '1' or watchdog;
			end if;
			assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
			--Check txData against exp_result.  May be a different case
			assert txData = exp_result(0) or txData = (exp_result(0) + X"20")
				report "txData (0x" & to_string(txData) & 
				") not equal to exp_result(0) (0x" & to_string(exp_result(0)) & ")." 
				severity failure;
			--In case txNow happened before deasserting seqDone, do it here.
			if i = 9 then
				--Watchdog time not updated since DR deasserted!
				wait until now >= watchdog_time + 10 ns;
				seqDone <= '0';
			end if;
			--Wait for txNow to go low again
			watchdog_time <= now;
			wait on txNow, watchdog;
			assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
			--Do it all again for exp_result(1)
			watchdog_time <= now;
			wait until txNow = '1' or watchdog;
			assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
			--Check txData against exp_result.  May be a different case
			assert txData = exp_result(1) or txData = (exp_result(1) + X"20")
				report "txData (0x" & to_string(txData) & 
				") not equal to exp_result(1) (0x" & to_string(exp_result(1)) & ")." 
				severity failure;
			--txNow deassert check
			watchdog_time <= now;
			wait on txNow, watchdog;
			assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
			--And again for exp_result(2)
			--May not occur on last iteration
			if i = 9 then
				--Pretty printing check, up to 10 characters
				for j in 1 to 10 loop
					watchdog_time <= now;
					wait on watchdog, txNow;
					if txNow = '1' then
						report "Pretty-printing item " & to_string(j) & "." severity note;
						watchdog_time <= now;
						wait on txNow, watchdog;
						assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100 ns." severity failure;
					else
						report "100 ns of no activity, presuming end of sequence" severity note;
						watchdog_time <= now;
						wait on watchdog;
						exit;
					end if;
				end loop;
			else
				--On i = 0 to 8: required space
				--txNow assertion check
				watchdog_time <= now;
				wait until txNow = '1' or watchdog;
				assert txNow = '1' and (not watchdog) report "txNow not asserted within 100 ns of seqDone." severity failure;
				--Check txData against exp_result (space = X"20").
				assert txData = exp_result(2) report "txData (0x" & to_string(txData) & 
					") not equal to exp_result(2) (0x" & to_string(exp_result(1)) & ")." 
					severity failure;
				watchdog_time <= now;
				wait on txNow, watchdog;
				assert txNow = '0' and (not watchdog) report "txNow not deasserted within 100ns." severity failure;
			end if;
		end loop;
		--Check that the thing doesn't print any more.
		wait until txNow = '1' or now > watchdog_time + 1000 ns;
		assert txNow = '0' report "Error: too much printing from txNow." severity failure;
		report "Test tb_cmdProc_a010 finished." severity note;
		finish(0);
	end process;

	txDone_respond : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				--At reset, tx module not ready
				txDone <= '0';
			elsif txDone = '0' then
				--Assert 1 cycle after deassert
				txDone <= '1';
			elsif txNow = '1' then
				--Deassert if we see txNow
				txDone <= '0';
			else
				--1 at all other times
				txDone <= '1';
			end if;
		end if;
	end process;

end tb_cmdProc_a010_arc;