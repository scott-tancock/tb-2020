library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;        -- for addition & counting
use ieee.numeric_std.all;               -- for type conversions
use ieee.math_real.all;                 -- for the ceiling and log constant calculation functions

library std;
use std.env.all;

use work.common_pack.all;

entity tb_cmdProc_accept is

end tb_cmdProc_accept;

architecture tb_cmdProc_accept_arc of tb_cmdProc_accept is

component cmdProc
	port (
		clk : std_logic;
		reset:		in std_logic;
		rxnow:		in std_logic;
		rxData:			in std_logic_vector (7 downto 0);
		txData:			out std_logic_vector (7 downto 0);
		rxdone:		out std_logic;
		ovErr:		in std_logic;
		framErr:	in std_logic;
		txnow:		out std_logic;
		txdone:		in std_logic;
		start: out std_logic;
		numWords_bcd: out BCD_ARRAY_TYPE(2 downto 0);
		dataReady: in std_logic;
		byte: in std_logic_vector(7 downto 0);
		maxIndex: in BCD_ARRAY_TYPE(2 downto 0);
		dataResults: in CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1);
		seqDone: in std_logic
	);
end component;

--Watchdog Timeout.
constant wdto : time := 100 ns;

signal clk : std_logic := '0';
signal rst : std_logic := '0';
signal rxNow : std_logic := '0';
--Capital A.  No chance of an ANNN, L or P since there's no Ns, Ls or Ps.
signal rxData : std_logic_vector(7 downto 0) := X"41";
signal rxDone : std_logic;
signal ovErr : std_logic := '0';
signal framErr : std_logic := '0';
signal txData : std_logic_vector(7 downto 0);
signal txNow : std_logic;
signal txDone : std_logic := '0';
signal start : std_logic;
signal numWords_bcd : BCD_ARRAY_TYPE(2 downto 0);
signal dataReady : std_logic := '0';
signal byte : std_logic_vector(7 downto 0) := X"00";
signal maxIndex : BCD_ARRAY_TYPE(2 downto 0) := (others => X"0");
signal dataResults : CHAR_ARRAY_TYPE(0 to RESULT_BYTE_NUM-1) := (others => X"00");
signal seqDone : std_logic := '0';
signal watchdog_time : time := 0 ns;
signal watchdog : boolean := false;

begin

	clk <= not clk after 5 ns;
	rst <= '0', '1' after 20 ns, '0' after 40 ns;
	watchdog <= now > watchdog_time + wdto;

	dut : cmdProc
		port map(
			clk => clk,
			reset => rst,
			rxnow => rxNow,
			rxData => rxData,
			txData => txData,
			rxdone => rxDone,
			ovErr => ovErr,
			framErr => framErr,
			txnow => txNow,
			txdone => txDone,
			start => start,
			numWords_bcd => numWords_bcd,
			dataReady => dataReady,
			byte => byte,
			maxIndex => maxIndex,
			dataResults => dataResults,
			seqDone => seqDone
		);

	--DO NOT write your design in this style.  It WILL NOT synthesise.
	rxNow_stimulate : process

	begin
		wait for 100 ns;
		assert rxDone = '0' report "Error: rxDone did not settle to 0" severity failure;
		rxNow <= '1';
		watchdog_time <= now;
		wait on watchdog, rxDone;
		assert rxDone = '1' report "Error: rxDone did not assert within 100 ns of rxNow" severity failure;
		rxNow <= '0';
		watchdog_time <= now;
		wait on watchdog, rxDone;
		assert rxDone = '0' report "Error: rxDone did not deassert within 100 ns of rxNow" severity failure;
		if now > 10 us then
			report "Test tb_cmdProc_accept finished" severity note;
			finish(0);
		end if;
	end process;

	txDone_respond : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				--At reset, tx module not ready
				txDone <= '0';
			elsif txDone = '0' then
				--Assert 1 cycle after deassert
				txDone <= '1';
			elsif txNow = '1' then
				--Deassert if we see txNow
				txDone <= '0';
			else
				--1 at all other times
				txDone <= '1';
			end if;
		end if;
	end process;


end tb_cmdProc_accept_arc;